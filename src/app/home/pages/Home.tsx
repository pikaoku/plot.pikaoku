import React from 'react'
import { Container } from 'semantic-ui-react'
import CampaignsTable from '../../campaign/components/CampaignsTable'

export default function Home() {
	return (
		<Container text>
			<CampaignsTable />
		</Container>
	)
}
