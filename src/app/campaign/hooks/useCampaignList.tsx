import React, { useState, useEffect } from 'react'
import { Campaign, CampaignKitchen } from '../../../logic/campaign/Campaign'

export default function useCampaignList(): Campaign[] {
	const [campaigns, setCampaigns] = useState<Campaign[]>([])

	useEffect(
		() =>
			setCampaigns([
				CampaignKitchen.new(),
				CampaignKitchen.new(),
				CampaignKitchen.new(),
			]),
		[]
	)

	return campaigns
}
