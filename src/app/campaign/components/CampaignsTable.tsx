import React from 'react'
import {
  Container,
  Table,
  TableHeader,
  TableBody,
  TableHeaderCell,
  TableRow,
  TableCell,
  Divider
} from 'semantic-ui-react'
import useCampaignList from '../hooks/useCampaignList'
import AddCampaignModal from './AddCampaignModal'

export default function CampaignsTable() {
  const campaigns = useCampaignList()
  return (
    <Container text>
      <Table>
        <TableHeader>
          <TableRow>
            <TableHeaderCell>Campaign Title</TableHeaderCell>
          </TableRow>
        </TableHeader>
        <TableBody>
          {campaigns.map(campaign => (
            <TableRow key={Math.random()}>
              <TableCell>{campaign.title.short}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Divider />
      <AddCampaignModal />
    </Container>
  )
}
