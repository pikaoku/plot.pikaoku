import React from 'react'
import { Formik } from 'formik'
import {
	Form,
	FormGroup,
	FormInput,
	Divider,
	TextArea,
	FormTextArea,
	Message,
} from 'semantic-ui-react'
import { CampaignKitchen, Campaign } from '../../../logic/campaign/Campaign'

function validateCampaignForm(values: Campaign) {
	let errors = {} as Partial<Campaign>
	if (values.title.full.length === 0) {
		errors.title = {
			full: 'Required',
			short: '',
			sub: '',
		}
	}
	return errors
}

export default function AddCampaignForm() {
	return (
		<Formik
			initialValues={CampaignKitchen.new()}
			validate={validateCampaignForm}
			validateOnBlur
			onSubmit={(values: Campaign, { setSubmitting }: any) => {
				setTimeout(() => {
					alert(JSON.stringify(values, null, 2))
					setSubmitting(false)
				}, 400)
			}}
		>
			{({
				values,
				errors = {},
				touched,
				handleChange,
				handleBlur,
				handleSubmit,
				isSubmitting,
				/* and other goodies */
			}) => (
				<Form>
					<FormInput
						label='Full Title'
						id='title.full'
						value={values.title.full}
						onChange={handleChange}
						onBlur={handleBlur}
						error={!!errors.title && !!errors.title.full}
						autoFocus
					/>
					<FormGroup widths='equal'>
						<FormInput
							label='Short Title'
							id='title.short'
							value={values.title.short}
							onChange={handleChange}
							onBlur={handleBlur}
						/>
						<FormInput
							label='Sub Title'
							id='title.sub'
							value={values.title.sub}
							onChange={handleChange}
							onBlur={handleBlur}
						/>
					</FormGroup>
					<Divider />
					<FormInput
						label='Hook'
						id='hook'
						value={values.hook}
						onChange={handleChange}
						onBlur={handleBlur}
					/>
					<FormTextArea
						label='Description'
						id='description'
						value={values.description}
						onChange={handleChange}
						onBlur={handleBlur}
					/>
					<Divider />
				</Form>
			)}
		</Formik>
	)
}
