import React, { useState } from 'react'
import {
  Button,
  Modal,
  ModalHeader,
  ModalContent,
  ModalActions
} from 'semantic-ui-react'
import AddCampaignForm from './AddCampaignForm'

export default function AddCampaignModal() {
  const [open, setOpen] = useState(false)
  const onOpen = () => setOpen(true)
  const onClose = () => setOpen(false)
  return (
    <>
      <Button positive content='Add Campaign' icon='add' onClick={onOpen} />
      <Modal open={open} onClose={onClose} size='small'>
        <ModalHeader content='Add Campaign' />
        <ModalContent>
          <AddCampaignForm />
        </ModalContent>
        <ModalActions>
          <Button content='Cancel' onClick={onClose} />
          <Button content='Save' onClick={onClose} positive />
        </ModalActions>
      </Modal>
    </>
  )
}
