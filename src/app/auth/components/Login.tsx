import React from 'react'

import { StyledFirebaseAuth } from 'react-firebaseui'
import FB from '../../../api/firebase/FB'

const Login = () => {
	const authConfig: any = {
		signInFlow: 'popup',
		signInSuccessUrl: '',
		signInOptions: [FB.types.auth.GoogleAuthProvider.PROVIDER_ID],
	}
	return <StyledFirebaseAuth uiConfig={authConfig} firebaseAuth={FB.auth} />
}

export default Login
