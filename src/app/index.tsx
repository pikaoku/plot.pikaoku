import React from 'react'
import { Helmet } from 'react-helmet'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Home from './home/pages/Home'

const App = () => {
	return (
		<>
			<Helmet>{/*  How're ya now  */}</Helmet>
			<BrowserRouter>
				<Switch>
					<Route exact path='/' component={Home} />
				</Switch>
			</BrowserRouter>
		</>
	)
}

export default App
