import { HasIdentifier } from '../common/Identifier'
import { Recipe, Prep, kitch } from 'kitch'

export type UserID = string // placeholder for if a more specific type of user id is added

export interface User extends HasIdentifier {
	username: string
	userId: UserID
}

export interface HasOwners {
	owners: UserID[]
}

const recipeUserEmpty: Recipe<User> = {
	id: Prep.empty.string,
	username: Prep.empty.string,
	userId: Prep.empty.string,
}

export const UserKitchen = kitch(recipeUserEmpty)
