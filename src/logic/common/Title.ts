import { Recipe, Prep, kitch } from 'kitch'

export interface Title {
	full: string
	short: string
	sub: string
}

export interface HasTitle {
	title: Title
}

const recipe: Recipe<Title> = {
	full: Prep.empty.string,
	short: Prep.empty.string,
	sub: Prep.empty.string,
}

export const TitleKitchen = kitch(recipe)
