// Placeholder incase a more specific requirement for uniqid is added later
export type uniqId = string

export interface HasIdentifier {
  id: uniqId
}
