import { Recipe, Prep, kitch } from 'kitch'

export interface Summarized {
	name: string
	short: string
	full: string
}

export const summarizedRecipe: Recipe<Summarized> = {
	name: Prep.empty.string,
	short: Prep.empty.string,
	full: Prep.empty.string,
}

export const SummarizedKitchen = kitch(summarizedRecipe)
