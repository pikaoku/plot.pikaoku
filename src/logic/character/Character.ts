import { HasIdentifier } from '../common/Identifier'
import { Recipe, Prep, kitch } from 'kitch'

export enum CharacterGender {
	Male,
	Female,
	None,
	Other,
}

export interface Character extends HasIdentifier {
	name: string
	race: string
	gender: CharacterGender
	notes: string
}

export interface HasCharacters {
	characters: Character[]
}

const recipe: Recipe<Character> = {
	id: Prep.empty.string,
	name: Prep.empty.string,
	race: Prep.empty.string,
	gender: () => CharacterGender.Other,
	notes: Prep.empty.string,
}

export const CharacterKitchen = kitch(recipe)
