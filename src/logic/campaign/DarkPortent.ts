import { Recipe, Prep, kitch } from 'kitch'

export interface Portent {
	complete: boolean
	description: string
	name: string
}

export interface HasPortents {
	portents: Portent[]
}

const recipe: Recipe<Portent> = {
	complete: Prep.empty.false,
	description: Prep.empty.string,
	name: Prep.empty.string,
}

export const PortentKitchen = kitch(recipe)
