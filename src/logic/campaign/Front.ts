import { HasPortents } from './DarkPortent'
import { Recipe, Prep, kitch } from 'kitch'

export interface Front extends HasPortents {
	name: string
	goal: string
}

export interface HasFronts {
	fronts: Front[]
}

const recipe: Recipe<Front> = {
	name: Prep.empty.string,
	goal: Prep.empty.string,
	portents: Prep.empty.array,
}

export const FrontKitchen = kitch(recipe)
