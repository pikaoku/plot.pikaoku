import { HasTitle, TitleKitchen } from '../common/Title'
import { HasFronts } from './Front'
import { HasOwners } from '../user/User'
import { HasIdentifier } from '../common/Identifier'

import { cook, Cooker, Recipe, Prep, Kitchen } from 'kitch'

export interface Campaign
	extends HasTitle,
		HasFronts,
		HasOwners,
		HasIdentifier {
	hook: string
	description: string
	truths: string[]
}

export interface FromCampaign {
	campaign: {
		name: string
		id: string
	}
}

export const recipeFromCampignEmpty: Recipe<FromCampaign> = {
	campaign: () => ({
		name: Prep.empty.string(),
		id: Prep.empty.string(),
	}),
}

const recipe: Recipe<Campaign> = {
	id: Prep.empty.string,
	title: TitleKitchen.new,
	hook: Prep.empty.string,
	description: Prep.empty.string,
	truths: Prep.empty.array,
	fronts: Prep.empty.array,
	owners: Prep.empty.array,
}

export const CampaignKitchen = kitch(recipe)
