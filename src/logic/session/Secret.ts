import { Summarized, summarizedRecipe } from '../common/Summarized'
import { Recipe, Prep, kitch } from 'kitch'

export interface Secret extends Summarized {
	shared: boolean
}

export interface HasSecrets {
	secrets: Secret[]
}

const recipe: Recipe<Secret> = {
	...summarizedRecipe,
	shared: Prep.empty.false,
}

export const SecretKitchen = kitch(recipe)
