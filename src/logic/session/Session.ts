import { HasSecrets } from './Secret'
import { HasTitle, TitleKitchen } from '../common/Title'
import { Summarized, SummarizedKitchen } from '../common/Summarized'
import { FromCampaign, recipeFromCampignEmpty } from '../campaign/Campaign'
import { HasIdentifier } from '../common/Identifier'
import { Recipe, Prep, kitch } from 'kitch'

export interface Session
	extends HasSecrets,
		HasTitle,
		HasIdentifier,
		FromCampaign {
	strongStart: Summarized
	scenes: Summarized[]
	description: string
	recap: string
	locations: Summarized[]
}

const recipeSessionEmpty: Recipe<Session> = {
	id: Prep.empty.string,
	title: TitleKitchen.new,
	description: Prep.empty.string,
	recap: Prep.empty.string,
	strongStart: SummarizedKitchen.new,
	secrets: Prep.empty.array,
	scenes: Prep.empty.array,
	locations: Prep.empty.array,
	...recipeFromCampignEmpty,
}

export const SessionKitchen = kitch(recipeSessionEmpty)
