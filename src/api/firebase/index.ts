import FB from './FB'
import { uniqId, HasIdentifier } from '../../logic/common/Identifier'

type Snapshot = firebase.firestore.DocumentSnapshot

export function firebaseFearer<Model extends HasIdentifier>(
	collection: string,
	makeDoc: Maker<Model>
) {
	// TODO: Research if this will be instantiated far too often.
	console.count(`Called Fearer for ${collection}`)
	const getColRef = () => FB.DB.collection(FB.BASE_PATH + collection)
	const getDocRef = (id: uniqId) => getColRef().doc(id)
	const extractData = (doc: Snapshot) =>
		makeDoc({ ...doc.data(), id: doc.id } as Partial<Model>)
	const wrap = (p: Promise<Snapshot>): Promise<Model> => p.then(extractData)
	return {
		fetch: (id: uniqId) => wrap(getDocRef(id).get()),
		edit: (id: uniqId, data: Partial<Model>) => getDocRef(id).update(data),
		add: (data: Partial<Model>) =>
			wrap(
				getColRef()
					.add(makeDoc(data))
					.then(ref => ref.get())
			),
		remove: (id: uniqId) => getDocRef(id).delete(),
	}
}
