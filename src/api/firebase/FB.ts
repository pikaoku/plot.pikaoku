import * as firebase from 'firebase/app'

import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
	apiKey: 'AIzaSyAsdpKiWccQ0iCbJqdR1WJVMfFWtEXffA0',
	authDomain: 'plot-pikaoku.firebaseapp.com',
	databaseURL: 'https://plot-pikaoku.firebaseio.com',
	projectId: 'plot-pikaoku',
	storageBucket: 'plot-pikaoku.appspot.com',
	messagingSenderId: '448023025332',
	appId: '1:448023025332:web:c5e40e6e816813653969e1',
}

const app = firebase.initializeApp(firebaseConfig)
const types = firebase
const DB = firebase.firestore(app)
const auth = firebase.auth(app)
const storage = firebase.storage(app)

DB.enablePersistence().catch(err => {
	switch (err.code) {
		case 'failed-precondition':
			// Multiple Tabs
			break
		case 'unimplemented':
			break
		default:
			break
	}
})

export type dboperator = '<' | '<=' | '==' | '>=' | '>' | 'array-contains'
export type dbwhere = [string, dboperator, string | number | boolean]

const DB_BASE_PATH = 'sites/plots/'

export default {
	types,
	app,
	DB,
	auth,
	storage,
	BASE_PATH: DB_BASE_PATH,
}
