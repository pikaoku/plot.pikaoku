import { firebaseFearer } from './firebase'

const api: PlotAPI = {
	users: {
		...firebaseFearer<User>('users', makeUser),
		list: {
			active$: () => {}
		}
	},
	campaigns: firebaseFearer<Campaign>('campaigns', makeCampaign),
	sessions: firebaseFearer<Session>('sessions', makeSession),
	characters: firebaseFearer<Character>('characters', makeCharacter),
}

export default api
